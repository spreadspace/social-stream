### Build Image
FROM golang:1.18-bullseye

RUN set -e \
  && apt-get update -q \
  && apt-get install -y -q --no-install-recommends make  \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY Makefile go.mod go.sum /usr/src/
COPY api /usr/src/api
COPY cmd /usr/src/cmd
COPY inventory /usr/src/inventory
COPY ui /usr/src/ui

WORKDIR /usr/src
RUN make build-static

### APP Image
FROM scratch
LABEL maintainer="Christian Pointner <equinox@spreadspace.org>"

COPY contrib/for-docker.yml /config.yml
COPY --from=0 /usr/src/social-stream /social-stream

EXPOSE 8000/tcp

CMD ["/social-stream", "/config.yml"]
