//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package inventory

import (
	"context"
	"fmt"
	"math/rand"

	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

const (
	controlLabelKey   = "k8s.spreadspace.org/created-by"
	controlLabelValue = "social-stream"

	annotationsSourceNameKey     = "k8s.spreadspace.org/social-stream-source-name"
	annotationsSourceRevisionKey = "k8s.spreadspace.org/social-stream-source-revision"
)

type streamInventory struct {
	namespace string
	clientset *kubernetes.Clientset
	indexer   cache.Indexer
	informer  cache.Controller
}

func obj2ReplicaSet(obj interface{}) (*appsv1.ReplicaSet, error) {
	rs, ok := obj.(*appsv1.ReplicaSet)
	if !ok {
		return nil, ErrInvalidK8sObject
	}
	return rs, nil
}

func metaUIDIndexFunc(obj interface{}) ([]string, error) {
	meta, err := meta.Accessor(obj)
	if err != nil {
		return []string{""}, fmt.Errorf("object has no meta: %v", err)
	}
	return []string{string(meta.GetUID())}, nil
}

func newStreamInventory(clientset *kubernetes.Clientset, namespace string) (si *streamInventory, err error) {
	si = &streamInventory{}
	si.namespace = namespace
	si.clientset = clientset

	optionsModifier := func(options *metav1.ListOptions) {
		labelSet := labels.Set(map[string]string{controlLabelKey: controlLabelValue})
		options.LabelSelector = labelSet.String()
	}
	rsListWatcher := cache.NewFilteredListWatchFromClient(clientset.AppsV1().RESTClient(), "replicasets", namespace, optionsModifier)
	indexers := cache.Indexers{}
	indexers["uid"] = metaUIDIndexFunc
	si.indexer, si.informer = cache.NewIndexerInformer(rsListWatcher, &appsv1.ReplicaSet{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			rs, err := obj2ReplicaSet(obj)
			if err == nil {
				fmt.Printf("inventory: stream %s created\n", rs.UID) // TODO: infoLog
			} else {
				fmt.Printf("ERROR: got invalid object from kubernetes: %T", obj) // TODO: errLog
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			rs, err := obj2ReplicaSet(new)
			if err == nil {
				fmt.Printf("inventory: stream %s updated\n", rs.UID) // TODO: infoLog
			} else {
				fmt.Printf("ERROR: got invalid object from kubernetes: %T", new) // TODO: errLog
			}
		},
		DeleteFunc: func(obj interface{}) {
			rs, err := obj2ReplicaSet(obj)
			if err == nil {
				fmt.Printf("inventory: stream %s deleted\n", rs.UID) // TODO: infoLog
			} else {
				fmt.Printf("ERROR: got invalid object from kubernetes: %T", obj) // TODO: errLog
			}
		},
	}, indexers)

	return
}

func (si *streamInventory) run(stop <-chan struct{}) {
	defer runtime.HandleCrash()
	go si.informer.Run(stop)
	<-stop
}

func newStreamFromReplicaset(rs *appsv1.ReplicaSet) *Stream {
	s := &Stream{}
	s.ID = string(rs.UID)
	s.Source.Name = rs.ObjectMeta.Annotations[annotationsSourceNameKey]
	s.Source.Revision = rs.ObjectMeta.Annotations[annotationsSourceRevisionKey]

	if rs.Status.Replicas > 0 {
		if rs.Status.FullyLabeledReplicas > 0 {
			s.State = StreamStarting
		}
		if rs.Status.ReadyReplicas > 0 {
			s.State = StreamRunning
		}
	} else {
		s.State = StreamStopping
	}
	// TODO: improve this - we likely need the pods info for this as well...
	return s
}

func generateName() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

	b := make([]rune, 8)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func (si *streamInventory) new(src *Source, output string) (*Stream, error) {
	name := generateName()
	replicas := int32(1)
	terminationGracePeriodSeconds := int64(0)
	rs := &appsv1.ReplicaSet{
		ObjectMeta: metav1.ObjectMeta{
			Name: "social-stream-" + name,
			Labels: map[string]string{
				controlLabelKey: controlLabelValue,
			},
			Annotations: map[string]string{
				annotationsSourceNameKey:     src.Name,
				annotationsSourceRevisionKey: src.Revision,
			},
		},
		Spec: appsv1.ReplicaSetSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"social-stream": name,
				},
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"social-stream": name,
					},
				},
				Spec: apiv1.PodSpec{
					TerminationGracePeriodSeconds: &terminationGracePeriodSeconds,
					Containers: []apiv1.Container{
						{
							Name:            src.Name,
							Image:           src.Config.Image,
							ImagePullPolicy: "Always",
							Command:         []string{src.Config.Command},
							Args:            append(src.Config.Args, output),
						},
					},
				},
			},
		},
	}

	client := si.clientset.AppsV1().ReplicaSets(si.namespace)
	result, err := client.Create(context.TODO(), rs, metav1.CreateOptions{}) // TODO: pass on context?
	if err != nil {
		return nil, err
	}

	return newStreamFromReplicaset(result), nil
}

func (si *streamInventory) list() (Streams, error) {
	objs := si.indexer.List()

	streams := Streams{}
	for _, obj := range objs {
		rs, err := obj2ReplicaSet(obj)
		if err != nil {
			return nil, err
		}
		streams = append(streams, newStreamFromReplicaset(rs))
	}
	return streams, nil
}

func (si *streamInventory) get(id string) (*Stream, error) {
	objs, _ := si.indexer.ByIndex("uid", id) // TODO: error handling (only possible if "uid" index does not exist..
	if len(objs) < 1 {
		return nil, ErrNotFound
	}
	if len(objs) > 1 {
		return nil, ErrMultipleK8sObjects
	}

	rs, err := obj2ReplicaSet(objs[0])
	if err != nil {
		return nil, err
	}
	return newStreamFromReplicaset(rs), nil
}

func (si *streamInventory) delete(id string) error {
	objs, _ := si.indexer.ByIndex("uid", id) // TODO: error handling (only possible if "uid" index does not exist..
	if len(objs) < 1 {
		return ErrNotFound
	}
	if len(objs) > 1 {
		return ErrMultipleK8sObjects
	}

	rs, err := obj2ReplicaSet(objs[0])
	if err != nil {
		return err
	}
	client := si.clientset.AppsV1().ReplicaSets(si.namespace)
	propagationPolicy := metav1.DeletePropagationForeground
	deleteOpts := metav1.DeleteOptions{
		PropagationPolicy: &propagationPolicy,
	}
	return client.Delete(context.TODO(), rs.ObjectMeta.Name, deleteOpts) // TODO: pass on context
}

// Public Interfaces

func (in *Inventory) NewStream(source, output string) (*Stream, error) {
	src, err := in.sources.getLatest(source)
	if err != nil {
		return nil, err
	}
	return in.streams.new(src, output)
}

func (in *Inventory) ListStreams() (Streams, error) {
	return in.streams.list()
}

func (in *Inventory) GetStream(id string) (*Stream, error) {
	return in.streams.get(id)
}

func (in *Inventory) DeleteStream(id string) error {
	return in.streams.delete(id)
}
