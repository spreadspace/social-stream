//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package inventory

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"os"
	"sync"

	"gopkg.in/yaml.v3"
)

type source struct {
	generation uint
	current    string
	revisions  map[string]*SourceConfig
}

type sourceInventory struct {
	mutex      sync.RWMutex
	generation uint
	sources    map[string]*source
}

func newSourceInventory() (si *sourceInventory) {
	si = &sourceInventory{}
	si.sources = make(map[string]*source)
	return
}

func (si *sourceInventory) addOrUpdateSource(name string, config *SourceConfig, hash hash.Hash) {
	configJson, _ := json.Marshal(config) // TODO: ignoring errors for now
	hash.Reset()
	hash.Write(configJson)
	configHash := base64.RawURLEncoding.EncodeToString(hash.Sum(nil))

	s, exists := si.sources[name]
	if !exists {
		s = &source{}
		s.revisions = make(map[string]*SourceConfig)
		si.sources[name] = s
	}
	s.generation = si.generation
	s.current = configHash
	if _, exists = s.revisions[configHash]; !exists {
		s.revisions[configHash] = config
	}
}

func (si *sourceInventory) update(new map[string]*SourceConfig, hash hash.Hash) error {
	si.mutex.Lock()
	defer si.mutex.Unlock()

	si.generation++
	for name, config := range new {
		si.addOrUpdateSource(name, config, hash)
	}

	return nil
}

func (si *sourceInventory) list() (sources Sources, err error) {
	si.mutex.RLock()
	defer si.mutex.RUnlock()

	sources = Sources{}
	for name, src := range si.sources {
		if src.generation != si.generation {
			continue
		}
		s := &Source{Name: name}
		s.Revision = src.current
		s.Config = *src.revisions[src.current]
		sources = append(sources, s)
	}
	return
}

func (si *sourceInventory) getLatest(name string) (source *Source, err error) {
	si.mutex.RLock()
	defer si.mutex.RUnlock()

	s, exists := si.sources[name]
	if !exists {
		err = ErrNotFound
		return
	}
	source = &Source{Name: name}
	source.Revision = s.current
	source.Config = *s.revisions[s.current]
	return
}

func (si *sourceInventory) getRevision(name, revision string) (source *Source, err error) {
	si.mutex.RLock()
	defer si.mutex.RUnlock()

	s, exists := si.sources[name]
	if !exists {
		err = ErrNotFound
		return
	}
	r, exists := s.revisions[revision]
	if !exists {
		err = ErrNotFound
		return
	}
	source = &Source{Name: name}
	source.Revision = revision
	source.Config = *r
	return
}

func (si *sourceInventory) rescanSourcesFile(sourcesFile string, hash hash.Hash) error {
	f, err := os.Open(sourcesFile)
	if err != nil {
		return fmt.Errorf("failed to open sources file(" + sourcesFile + "): " + err.Error())
	}
	defer f.Close()

	data := make(map[string]*SourceConfig)
	decoder := yaml.NewDecoder(f)
	decoder.KnownFields(true)
	if err = decoder.Decode(data); err != nil {
		return fmt.Errorf("failed to parse sources file(" + sourcesFile + "): " + err.Error())
	}

	return si.update(data, hash)
}

// Public Interfaces

func (in *Inventory) ListSources() (sources Sources, err error) {
	return in.sources.list()
}

func (in *Inventory) GetSource(name, revision string) (source *Source, err error) {
	if revision != "" {
		return in.sources.getRevision(name, revision)
	}
	return in.sources.getLatest(name)
}
