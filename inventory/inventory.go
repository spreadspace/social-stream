//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package inventory

import (
	"hash"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/crypto/blake2s"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type Inventory struct {
	infoLog *log.Logger
	errLog  *log.Logger
	dbgLog  *log.Logger
	conf    *Config
	hash    hash.Hash
	sources *sourceInventory
	streams *streamInventory
	k8s     *kubernetes.Clientset
}

func NewInventory(c *Config, infoLog, errLog, dbgLog *log.Logger) (in *Inventory, err error) {
	if infoLog == nil {
		infoLog = log.New(ioutil.Discard, "", 0)
	}
	if errLog == nil {
		errLog = log.New(ioutil.Discard, "", 0)
	}
	if dbgLog == nil {
		dbgLog = log.New(ioutil.Discard, "", 0)
	}

	in = &Inventory{conf: c}
	in.infoLog = infoLog
	in.errLog = errLog
	in.dbgLog = dbgLog
	in.hash, err = blake2s.New128([]byte{0xDE, 0xAD, 0xBE, 0xEF}) // we don't really care for a secure hash
	if err != nil {
		return
	}

	if err = in.initK8sClient(c); err != nil {
		return
	}

	in.sources = newSourceInventory()
	in.streams, err = newStreamInventory(in.k8s, "default") // TODO: hardcoded namespace
	return
}

func (in *Inventory) initK8sClient(c *Config) (err error) {
	var k8sConfig *rest.Config
	if c.Kubernetes.InCluster {
		k8sConfig, err = rest.InClusterConfig()
	} else {
		if c.Kubernetes.KubeConfig != "" {
			k8sConfig, err = clientcmd.BuildConfigFromFlags("", c.Kubernetes.KubeConfig)
		} else {
			home := os.Getenv("HOME") // unix
			if home == "" {
				home = os.Getenv("USERPROFILE") // windows
			}
			k8sConfig, err = clientcmd.BuildConfigFromFlags("", filepath.Join(home, ".kube", "config"))
		}
	}
	if err != nil {
		return
	}

	if in.k8s, err = kubernetes.NewForConfig(k8sConfig); err != nil {
		return
	}

	var k8sVersion *version.Info
	if k8sVersion, err = in.k8s.ServerVersion(); err != nil {
		return
	}
	in.infoLog.Printf("inventory: successfully connected to kubernetes cluster %s/%s (version %s)", k8sConfig.Host, k8sConfig.APIPath, k8sVersion)
	return
}

func (in *Inventory) runSourceReloader(stop <-chan struct{}) {
	in.infoLog.Printf("inventory: starting source file reloader for: %s", in.conf.Sources.File)
	defer in.infoLog.Printf("inventory: source file reloader stopped")

	if err := in.sources.rescanSourcesFile(in.conf.Sources.File, in.hash); err != nil {
		in.errLog.Printf("inventory: failed to load sources: %v", err)
	}

	ticker := time.NewTicker(in.conf.Sources.Reload)
	for {
		select {
		case <-stop:
			return
		case <-ticker.C:
			if err := in.sources.rescanSourcesFile(in.conf.Sources.File, in.hash); err != nil {
				in.errLog.Printf("inventory: failed to reload sources: %v", err)
			}
		}
	}
}

func (in *Inventory) Start(stop <-chan struct{}) {
	go in.runSourceReloader(stop)
	go in.streams.run(stop)
}
