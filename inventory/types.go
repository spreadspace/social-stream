//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package inventory

import (
	"errors"
)

//******* Errors

var (
	ErrNotImplemented     = errors.New("not implemented")
	ErrNotFound           = errors.New("not found")
	ErrInvalidK8sObject   = errors.New("got invalid object from kubernetes")
	ErrMultipleK8sObjects = errors.New("found multiple kubernetes objects")
)

//******* Sources

type SourceConfig struct {
	Image   string   `json:"image" yaml:"image"`
	Command string   `json:"command" yaml:"command"`
	Args    []string `json:"args" yaml:"args"`
}

type Source struct {
	Name     string       `json:"name"`
	Revision string       `json:"revision"`
	Config   SourceConfig `json:"config"`
}

type Sources []*Source

//******* Streams

type StreamState int

const (
	StreamNew StreamState = iota
	StreamStarting
	StreamRunning
	StreamStopping
)

func (s StreamState) String() string {
	switch s {
	case StreamNew:
		return "new"
	case StreamStarting:
		return "starting"
	case StreamRunning:
		return "running"
	case StreamStopping:
		return "stopping"
	}
	return "unknown"
}

func (s *StreamState) fromString(str string) error {
	switch str {
	case "new":
		*s = StreamNew
	case "starting":
		*s = StreamStarting
	case "running":
		*s = StreamRunning
	case "stopping":
		*s = StreamStopping
	default:
		return errors.New("invalid stream state: '" + str + "'")
	}
	return nil
}

func (s StreamState) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

func (s *StreamState) UnmarshalText(data []byte) (err error) {
	return s.fromString(string(data))
}

type Stream struct {
	ID     string      `json:"id"`
	State  StreamState `json:"state"`
	Source struct {
		Name     string `json:"name"`
		Revision string `json:"revision"`
	} `json:"source"`
}

type Streams []*Stream

//*******
