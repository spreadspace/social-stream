//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package inventory

import (
	"time"
)

type SourcesConfig struct {
	File   string        `json:"file" yaml:"file" toml:"file"`
	Reload time.Duration `json:"reload" yaml:"reload" toml:"reload"`
}

type KubernetesConfig struct {
	InCluster  bool   `json:"in-cluster" yaml:"in-cluster" toml:"in-cluster"`
	KubeConfig string `json:"kube-config" yaml:"kube-config" toml:"kube-config"`
}

type Config struct {
	Sources    SourcesConfig    `json:"sources" yaml:"sources" toml:"sources"`
	Kubernetes KubernetesConfig `json:"kubernetes" yaml:"kubernetes" toml:"kubernetes"`
}
