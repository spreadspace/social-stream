# simple stream feed scheduler

## Requirements

* [Go](https://golang.org/) (v1.17+ recommended)

## Quickstart

To install tank, use the `go get` command:

```sh
$ go get gitlab.com/spreadspace/social-stream/...
```
