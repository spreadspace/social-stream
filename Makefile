##
##  social-stream, a simple stream feed scheduler
##
##
##  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
##
##  This file is part of social-stream.
##
##  social-stream is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  social-stream is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
##

GOCMD := go
ifdef GOROOT
GOCMD = $(GOROOT)/bin/go
endif

EXECUTEABLE := social-stream

all: build
.PHONY: vet format ui build clean distclean

vet:
	$(GOCMD) vet ./...

format:
	$(GOCMD) fmt ./...

ui:
	$(GOCMD) generate ./ui

build: ui
	$(GOCMD) build -o $(EXECUTEABLE) ./cmd/social-stream

build-static: ui
	CGO_ENABLED=0 $(GOCMD) build -ldflags '-extldflags "-static"' -o $(EXECUTEABLE) ./cmd/social-stream

dev:
	$(GOCMD) build -o $(EXECUTEABLE) -tags=dev ./cmd/social-stream

clean:
	rm -f $(EXECUTEABLE)

distclean: clean
	rm -f ui/assets_vfsdata.go
