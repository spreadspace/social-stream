//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/spreadspace/social-stream/inventory"
)

func getStdoutLog(prefix string) *log.Logger {
	return log.New(os.Stdout, prefix, 0)
}

func getStderrLog(prefix string) *log.Logger {
	return log.New(os.Stderr, prefix, 0)
}

func getDbgLog(prefix string) *log.Logger {
	dbg := log.New(ioutil.Discard, prefix, 0)
	if _, exists := os.LookupEnv("SOCIAL_STREAM_DEBUG"); exists {
		dbg.SetOutput(os.Stdout)
	}
	return dbg
}

var (
	infoLog = getStdoutLog("[ --- ] ")
	errLog  = getStderrLog("[ ERR ] ")
	dbgLog  = getDbgLog("[ DBG ] ")
)

func main() {
	if len(os.Args) != 2 {
		errLog.Printf("Usage %s <config-file>", os.Args[0])
		os.Exit(2)
	}

	conf, err := ReadConfig(os.Args[1])
	if err != nil {
		errLog.Printf("failed to read configuration: %v", err)
		os.Exit(2)
	}

	in, err := inventory.NewInventory(&conf.Inventory, infoLog, errLog, dbgLog)
	if err != nil {
		errLog.Printf("failed to initialize inventory: %v", err)
		os.Exit(1)
	}
	infoLog.Println("successfully initialized inventory")

	stop := make(chan struct{})
	defer close(stop)

	in.Start(stop)

	if err = runWeb(conf.Web, in); err != nil {
		os.Exit(1)
	}
}
