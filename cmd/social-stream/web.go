//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	apiV1 "gitlab.com/spreadspace/social-stream/api/v1"
	"gitlab.com/spreadspace/social-stream/inventory"
	"gitlab.com/spreadspace/social-stream/ui"
)

const (
	WebUIPathPrefix = "/ui/"
	WebAPIv1Prefix  = "/api/v1/"
)

func apache2CombinedLogger(param gin.LogFormatterParams) string {
	return fmt.Sprintf("%s - - [%s] \"%s %s %s\" %d %d \"-\" \"%s\"\n",
		param.ClientIP,
		param.TimeStamp.Format(time.RFC1123),
		param.Method,
		param.Path,
		param.Request.Proto,
		param.StatusCode,
		param.BodySize,
		param.Request.UserAgent(),
	)
}

func installLogger(r *gin.Engine, conf WebConfig) error {
	if conf.AccessLogs == "" {
		return nil
	}

	var logConfig gin.LoggerConfig
	target := strings.SplitN(conf.AccessLogs, ":", 2)
	switch strings.ToLower(target[0]) {
	case "stdout":
		logConfig.Output = os.Stdout
	case "stderr":
		logConfig.Output = os.Stderr
	case "file":
		if len(target) != 2 {
			return fmt.Errorf("please specify a path to the access log")
		}
		f, err := os.OpenFile(target[1], os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return fmt.Errorf("failed to open access log: %v", err)
		}
		logConfig.Output = f
		logConfig.Formatter = apache2CombinedLogger
	default:
		return fmt.Errorf("unknown log target type: %q", target[0])
	}

	r.Use(gin.LoggerWithConfig(logConfig))

	return nil
}

func runWebListener(ln net.Listener, conf WebConfig, in *inventory.Inventory) error {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(gin.Recovery())
	if err := installLogger(r, conf); err != nil {
		return err
	}
	r.HandleMethodNotAllowed = true

	r.GET("/", func(c *gin.Context) { c.Redirect(http.StatusSeeOther, WebUIPathPrefix) })
	r.StaticFS(WebUIPathPrefix, ui.Assets)

	apiV1.InstallHTTPHandler(r.Group(WebAPIv1Prefix), in, infoLog, errLog, dbgLog)

	srv := &http.Server{
		Handler:      r,
		WriteTimeout: 12 * time.Hour,
		ReadTimeout:  12 * time.Hour,
	}
	if conf.ReadTimeout > 0 {
		srv.ReadTimeout = conf.ReadTimeout
	}
	if conf.WriteTimeout > 0 {
		srv.WriteTimeout = conf.WriteTimeout
	}

	infoLog.Printf("web: listening on %s", ln.Addr())
	return srv.Serve(ln)
}

func runWeb(conf WebConfig, in *inventory.Inventory) error {
	ln, err := net.Listen("tcp", conf.Listen)
	if err != nil {
		errLog.Printf("web: failed to open socket, %v", err)
		return err
	}

	if err = runWebListener(ln, conf, in); err != nil {
		errLog.Printf("web: failed, %v", err)
	}
	return err
}
