//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"errors"
	"os"
	"time"

	"gitlab.com/spreadspace/social-stream/inventory"
	"gopkg.in/yaml.v3"
)

type WebConfig struct {
	Listen       string        `json:"listen" yaml:"listen" toml:"listen"`
	AccessLogs   string        `json:"access-logs" yaml:"access-logs" toml:"access-logs"`
	ReadTimeout  time.Duration `json:"read-timeout" yaml:"read-timeout" toml:"read-timeout"`
	WriteTimeout time.Duration `json:"write-timeout" yaml:"write-timeout" toml:"write-timeout"`
}

type Config struct {
	Web       WebConfig        `json:"web" yaml:"web" toml:"web"`
	Inventory inventory.Config `json:"inventory" yaml:"inventory" toml:"inventory"`
}

func ReadConfig(configfile string) (conf *Config, err error) {
	var f *os.File
	if f, err = os.Open(configfile); err != nil {
		err = errors.New("error opening config file(" + configfile + "): " + err.Error())
		return
	}
	defer f.Close()

	conf = &Config{}
	decoder := yaml.NewDecoder(f)
	decoder.KnownFields(true)
	if err = decoder.Decode(conf); err != nil {
		err = errors.New("error parsing config file(" + configfile + "): " + err.Error())
		return
	}
	if conf.Inventory.Sources.Reload <= 0 {
		conf.Inventory.Sources.Reload = 10 * time.Second
		infoLog.Printf("setting source reload time to %v", conf.Inventory.Sources.Reload)
	}
	infoLog.Printf("successfully read config file (%s)", configfile)
	return
}
