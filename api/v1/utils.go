//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package v1

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/spreadspace/social-stream/inventory"
)

var (
	ErrNotImplemented = errors.New("not implemented")
)

type SourcesListing struct {
	Sources inventory.Sources `json:"results"`
}

type StreamsListing struct {
	Streams inventory.Streams `json:"results"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

func statusCodeFromError(err error) (code int, response ErrorResponse) {
	code = http.StatusInternalServerError
	response = ErrorResponse{Error: err.Error()}

	switch err {
	case ErrNotImplemented:
		code = http.StatusNotImplemented

	case inventory.ErrNotImplemented:
		code = http.StatusNotImplemented
	case inventory.ErrNotFound:
		code = http.StatusNotFound
	case inventory.ErrInvalidK8sObject:
		code = http.StatusInternalServerError
	case inventory.ErrMultipleK8sObjects:
		code = http.StatusInternalServerError
	}
	return
}

func sendError(c *gin.Context, err error) {
	code, response := statusCodeFromError(err)
	c.JSON(code, response)
}
