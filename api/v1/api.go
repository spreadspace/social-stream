//
//  social-stream, a simple stream feed scheduler
//
//
//  Copyright (C) 2020 Christian Pointner <equinox@spreadspace.org>
//
//  This file is part of social-stream.
//
//  social-stream is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  social-stream is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with social-stream. If not, see <http://www.gnu.org/licenses/>.
//

package v1

import (
	"io/ioutil"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/spreadspace/social-stream/inventory"
)

type API struct {
	infoLog *log.Logger
	errLog  *log.Logger
	dbgLog  *log.Logger
	in      *inventory.Inventory
}

func NewAPI(in *inventory.Inventory, infoLog, errLog, dbgLog *log.Logger) (api *API) {
	if infoLog == nil {
		infoLog = log.New(ioutil.Discard, "", 0)
	}
	if errLog == nil {
		errLog = log.New(ioutil.Discard, "", 0)
	}
	if dbgLog == nil {
		dbgLog = log.New(ioutil.Discard, "", 0)
	}

	api = &API{}
	api.in = in
	api.infoLog = infoLog
	api.errLog = errLog
	api.dbgLog = dbgLog
	return
}

func InstallHTTPHandler(r *gin.RouterGroup, in *inventory.Inventory, infoLog, errLog, dbgLog *log.Logger) {
	api := NewAPI(in, infoLog, errLog, dbgLog)

	// Sources
	sources := r.Group("sources")
	{
		sources.GET("", api.ListSources)
		sources.GET(":source-name", api.GetSource)
		sources.GET(":source-name/:revision", api.GetSourceRevision)
	}
	// Streams
	streams := r.Group("streams")
	{
		streams.GET("", api.ListStreams)
		streams.POST("", api.CreateStream)
		streams.GET(":stream-id", api.GetStream)
		streams.DELETE(":stream-id", api.DeleteStream)
	}
}
