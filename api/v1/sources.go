//
//  tank
//
//  Import and Playlist Daemon for autoradio project
//
//
//  Copyright (C) 2017-2019 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of tank.
//
//  tank is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  tank is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with tank. If not, see <http://www.gnu.org/licenses/>.
//

package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (api *API) ListSources(c *gin.Context) {
	sources, err := api.in.ListSources()
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, SourcesListing{Sources: sources})
}

func (api *API) GetSource(c *gin.Context) {
	source, err := api.in.GetSource(c.Param("source-name"), "")
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, source)
}

func (api *API) GetSourceRevision(c *gin.Context) {
	source, err := api.in.GetSource(c.Param("source-name"), c.Param("revision"))
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, source)
}
