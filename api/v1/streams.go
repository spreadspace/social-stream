//
//  tank
//
//  Import and Playlist Daemon for autoradio project
//
//
//  Copyright (C) 2017-2019 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of tank.
//
//  tank is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  tank is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with tank. If not, see <http://www.gnu.org/licenses/>.
//

package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (api *API) CreateStream(c *gin.Context) {
	srcName := c.Query("source")
	if srcName == "" {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "source is mandatory"})
		return
	}

	// TODO: get this from request
	output := "rtmp://127.0.0.1/test/output"

	stream, err := api.in.NewStream(srcName, output)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, stream)
}

func (api *API) ListStreams(c *gin.Context) {
	streams, err := api.in.ListStreams()
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, StreamsListing{Streams: streams})
}

func (api *API) GetStream(c *gin.Context) {
	stream, err := api.in.GetStream(c.Param("stream-id"))
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, stream)
}

func (api *API) DeleteStream(c *gin.Context) {
	if err := api.in.DeleteStream(c.Param("stream-id")); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}
